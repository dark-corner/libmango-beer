#ifndef REMOTE_H
#define REMOTE_H

#include "mango_types.h"

#include <set>

#include <boost/asio/ip/tcp.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/string.hpp>

namespace mango {
using boost::asio::ip::tcp;

typedef enum RemoteCommand {
	COMMAND_KERNEL = 1,
	COMMAND_BUFFER,
	COMMAND_BUFFER_WRITE,
	COMMAND_BUFFER_READ,
	COMMAND_START_KERNEL,
	COMMAND_KERNEL_WAIT,
	COMMAND_TIMINGS,
	COMMAND_DETACH,
} mango_remote_cmd_t;

struct Response {
	mango_exit_code_t result;
	std::string       data;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int)
	{
		ar & result;
		ar & data;
	}
};

struct Request {
	std::string   appId;
	RemoteCommand cmd;
	uint32_t      id;
	size_t        size;
	std::string   buffer;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int)
	{
		ar & appId;
		ar & cmd;
		ar & id;
		ar & size;
		ar & buffer;
	}
};

class Device {
	public:
		explicit Device(
				const std::string & app_name,
				const std::string & host,
				const std::string & service);
		~Device();

		mango_exit_code_t sendBuffer(
				const mango_id_t id,
				const size_t size);
		mango_exit_code_t sendKernel(
				const mango_id_t id,
				const std::string & path);
		mango_exit_code_t readBuffer(
				void * GN_buffer,
				const mango_id_t id,
				const size_t size);
		mango_exit_code_t writeBuffer(
				const void * GN_buffer,
				const mango_id_t id,
				const size_t size);
		mango_exit_code_t waitKernelCompletion(
				const mango_id_t id);
		mango_exit_code_t startKernel(
				const mango_id_t id,
				const std::string & arguments);
		mango_exit_code_t detach();
		std::pair<long,long> fetchExecutionTime(const mango_id_t id);

		inline const std::string & app_name() const { return _appId; }

		inline const std::string & host() const { return _host; }

		inline const std::string & service() const { return _service; }

	private:
		template<typename T>
		void iserialize(T& t) {
			boost::archive::text_iarchive ia(_stream);
			ia >> t;
		}
		template<typename T>
		void oserialize(T& t) {
			boost::archive::text_oarchive oa(_stream);
			oa << t;
		}

		std::string _appId;
		std::string _host;
		std::string _service;
		tcp::iostream _stream;
};

} /* mango */

#endif /* ifndef REMOTE_H */
