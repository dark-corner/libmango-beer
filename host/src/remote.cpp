#include "remote.h"
#include "logger.h"

#include <fstream>

namespace mango {
	Device::Device(
			const std::string & app_name,
			const std::string & host,
			const std::string & service)
			: _appId(app_name)
			, _host(host)
			, _service(service) {}

	Device::~Device() {}

	mango_exit_code_t Device::sendBuffer(const mango_id_t id, const size_t size) {
		Request req{_appId, COMMAND_BUFFER, id, size, ""};
		Response res;
		_stream.connect(_host, _service);

		mango_log->Info("registering buffer %d with remote device", id);
		oserialize(req);
		iserialize(res);
		_stream.close();

		return res.result;
	}

	mango_exit_code_t Device::sendKernel(
			const mango_id_t id,
			const std::string & path) {
		Request req{_appId, COMMAND_KERNEL, id, 0, ""};
		Response res;
		_stream.connect(_host, _service);

		std::ifstream ifs(path, std::ios::binary|std::ios::in);
		if (!ifs.good()) {
			throw std::runtime_error("path provided is not a valid file");
		}
		ifs.seekg(0, std::ios::end);
		req.buffer.resize(ifs.tellg());
		req.size = req.buffer.size();
		ifs.seekg(0, std::ios::beg);
		ifs.read(&req.buffer[0], req.size);
		ifs.close();
		mango_log->Info("sending kernel binary with size %d to remote device", req.size);
		oserialize(req);
		iserialize(res);
		_stream.close();

		return res.result;
	}

	mango_exit_code_t Device::readBuffer(
			void * GN_buffer,
			const mango_id_t id,
			const size_t size) {
		Request req{_appId, COMMAND_BUFFER_READ, id, size, ""};
		std::string res;
		_stream.connect(_host, _service);

		mango_log->Info("reading buffer %d from remote device", id);
		oserialize(req);
		iserialize(res);

		std::memcpy(GN_buffer, res.c_str(), size);
		_stream.close();
		return ExitCode::SUCCESS;
	}

	mango_exit_code_t Device::writeBuffer(
			const void * GN_buffer,
			const mango_id_t id,
			const size_t size) {
		Request req{_appId, COMMAND_BUFFER_WRITE, id, size, ""};
		Response res;
		_stream.connect(_host, _service);
		std::string buf((char*)GN_buffer, size);

		mango_log->Info("writing buffer %d to remote device", id);
		oserialize(req);
		oserialize(buf);
		iserialize(res);
		_stream.close();

		return res.result;
	}

	std::pair<long,long> Device::fetchExecutionTime(
			const mango_id_t id) {
		long start, end;
		Request req{_appId, COMMAND_TIMINGS, id, 0, ""};
		_stream.connect(_host, _service);

		oserialize(req);
		iserialize(start);
		iserialize(end);
		_stream.close();
		return std::make_pair(start, end);
	}

	mango_exit_code_t Device::waitKernelCompletion(const mango_id_t id) {
		Request req{_appId, COMMAND_KERNEL_WAIT, id, 0, ""};
		Response res;
		_stream.connect(_host, _service);

		mango_log->Info("wait for remote kernel completion");
		oserialize(req);
		iserialize(res);
		_stream.close();

		return res.result;
	}

	mango_exit_code_t Device::startKernel(
			const mango_id_t id,
			const std::string &arguments) {
		Request req{_appId, COMMAND_START_KERNEL, id, 0, arguments};
		Response res;
		_stream.connect(_host, _service);

		mango_log->Info("launching remote kernel");
		oserialize(req);
		iserialize(res);
		_stream.close();

		return res.result;
	}

	mango_exit_code_t Device::detach() {
		Request req{_appId, COMMAND_DETACH, 0, 0, ""};
		Response res;
		_stream.connect(_host, _service);

		mango_log->Info("detaching from remote device");
		oserialize(req);
		iserialize(res);
		_stream.close();
		return res.result;
	}

} /* mango */
